class InvalidRegistrationNumberError < StandardError
  def initialize(registration_number)
    super("Invalid registration number '#{registration_number}'")
  end
end

class InvalidSlotNumberError < ArgumentError
  def initialize(slot_number)
    super("Invalid slot number '#{slot_number}'")
  end
end

class InvalidVehicleError < ArgumentError
  def initialize(vehicle)
    super("Invalid vehicle of type '#{vehicle.class}'")
  end
end

class SlotAlreadyOccupiedError < StandardError
  def initialize(slot_number)
    super("Cannot park vehicle!! Parking slot ##{slot_number} is already occupied")
  end
end

class SlotAlreadyEmptyError < StandardError
  def initialize(slot_number)
    super("Cannot unpark vehicle!! Parking slot ##{slot_number} is already empty")
  end
end

class InvalidParkingLotSizeError < StandardError
  def initialize(size)
    super("Invalid size #{size} for parking lot")
  end
end

class ParkingLotFullError < StandardError
  def initialize
    super('Parking lot is already full')
  end
end

class InvalidSearchAttribute < ArgumentError
  def initialize(search_attribute)
    super("Invalid search attribute '#{search_attribute}'")
  end
end

class InvalidResponseAttribute < ArgumentError
  def initialize(response_attribute)
    super("Invalid response attribute '#{response_attribute}'")
  end
end
