require_relative File.join('..', 'errors')
require_relative 'vehicle'

class ParkingSlot
  attr_reader :slot_number, :vehicle

  def initialize(slot_number)
    @slot_number = slot_number
    validate
    @vehicle = nil
  end

  def park_vehicle(vehicle)
    raise InvalidVehicleError.new(vehicle) unless vehicle and Vehicle === vehicle
    raise SlotAlreadyOccupiedError.new(@slot_number) if occupied?

    @vehicle = vehicle
  end

  def occupied?
    !@vehicle.nil?
  end

  def unpark_vehicle
    raise SlotAlreadyEmptyError.new(@slot_number) unless occupied?

    vehicle = @vehicle
    @vehicle = nil
    vehicle
  end

  private

  def validate
    raise InvalidSlotNumberError.new(@slot_number) unless @slot_number and @slot_number >= 1
  end
end