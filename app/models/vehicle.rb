require_relative File.join('..', 'errors')
require_relative 'vehicle/registration_number'

class Vehicle

  attr_reader :registration_number, :colour

  def initialize(registration_number, color)
    @registration_number = RegistrationNumber.new(registration_number)
    @colour = color
  end

  def ==(another_vehicle)
    Vehicle === another_vehicle &&
    self.registration_number == another_vehicle.registration_number &&
        self.colour.downcase == another_vehicle.colour.downcase
  end
end