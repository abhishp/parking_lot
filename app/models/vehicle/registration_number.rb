require_relative File.join('..', '..', 'errors')

class Vehicle
  class RegistrationNumber
    REGISTRATION_NUMBER_REGEX = /\A(?<state_code>[a-z]{2})[-\s]?(?<district_code>\d{1,2})[-\s]?(?<prefix>[a-z]{1,3})[-\s]?(?<number>\d{3,4})\z/i
    attr_reader :state_code, :district_code, :prefix, :number

    def initialize(reg_num)
      match = reg_num.match(REGISTRATION_NUMBER_REGEX)
      raise InvalidRegistrationNumberError.new(reg_num) unless match
      @state_code = match['state_code'].upcase
      @district_code = match['district_code']
      @prefix = match['prefix'].upcase
      @number = match['number']
    end

    def to_s
      "#{@state_code}-#{@district_code}-#{@prefix}-#{@number}"
    end

    def ==(another_reg_num)
      self.to_s == another_reg_num.to_s.upcase
    end
  end
end
