require_relative 'errors'
require_relative 'models/parking_slot'
require_relative 'slot_search_provider'
require_relative File.join('..', 'lib', 'utils', 'min_heap')

class ParkingLot
  attr_reader :number_of_slots

  def initialize(number_of_slots)
    @number_of_slots = number_of_slots
    validate
    @available_slots_heap = Utils::MinHeap.new (1..number_of_slots).to_a
    @slots = {}
    @search_provider = SlotSearchProvider.new
  end

  def full?
    !@available_slots_heap.empty?
  end

  def park_vehicle(vehicle)
    raise ParkingLotFullError.new unless full?

    nearest_available_slot = @available_slots_heap.pop
    slot = ParkingSlot.new(nearest_available_slot)
    slot.park_vehicle(vehicle)
    @slots[nearest_available_slot] = slot
    @search_provider.index(slot)
    slot
  end

  def unpark_vehicle(slot_number)
    raise InvalidSlotNumberError.new(slot_number) unless slot_number.between?(1, @number_of_slots)
    raise SlotAlreadyEmptyError.new(slot_number) unless @slots.has_key?(slot_number)

    slot = @slots.delete slot_number
    @available_slots_heap.push(slot_number)
    vehicle = slot.unpark_vehicle
    @search_provider.remove_from_index(slot.slot_number, vehicle)
    vehicle
  end

  def filled_slots
    @slots.values.sort_by(&:slot_number)
  end

  def find(params = {})
    @slots.values_at *@search_provider.find(params)
  end

  private

  def validate
    raise InvalidParkingLotSizeError.new(@number_of_slots) unless @number_of_slots >= 1
  end
end