require_relative File.join('..', '..', 'errors')
require_relative File.join('..', '..', 'models', 'vehicle')
require_relative File.join('..', '..', '..', 'lib', 'utils', 'configuration')

module Input
  module Commands
    class ParkVehicle
      def initialize(registration_number, colour)
        @registration_number = registration_number
        @colour = colour
        configs = Utils::Configuration.instance
        @parking_lot_full_error_message = configs.parking_lot_full_error_message
      end

      def process(app_context)
        vehicle = Vehicle.new @registration_number, @colour
        slot_number = app_context.parking_lot.park_vehicle(vehicle).slot_number
        puts "Allocated slot number: #{slot_number}"
      rescue ParkingLotFullError
        puts @parking_lot_full_error_message
      rescue StandardError => e
        puts e.message
      end
    end
  end
end