require_relative File.join('..', '..', 'parking_lot')

module Input
  module Commands
    class CreateParkingLot
      def initialize(number_of_slots)
        @number_of_slots = number_of_slots.to_i
      end

      def process(app_context)
        app_context.parking_lot = ParkingLot.new(@number_of_slots)
        puts "Created a parking lot with #{@number_of_slots} slots"
      rescue StandardError => e
        puts e.message
      end
    end
  end
end