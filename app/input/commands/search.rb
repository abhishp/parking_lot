require_relative File.join('..', '..', 'errors')
require_relative File.join('..', '..', '..', 'lib', 'utils', 'configuration')

module Input
  module Commands
    class Search

      def initialize(attributes, *search_term)
        @search_params = {}
        @response_attribute = attributes['response_attribute'].to_s.chomp('s').to_sym

        search_attribute = attributes['search_attribute'].to_s.chomp('s').to_sym
        @search_params[search_attribute] = search_term.join(' ').split(/,\s*/)

        @empty_search_result_message = Utils::Configuration.instance.empty_search_result_message
      end

      def process(app_context)
        slots = app_context.parking_lot.find(@search_params)
        if slots.empty?
          puts @empty_search_result_message
        else
          print_attributes(slots)
        end
      rescue StandardError => e
        puts e.message
      end

      private

      def print_attributes(slots)
        test_subject = slots[0]
        if test_subject.respond_to? @response_attribute
          print_slot_attribute(slots)
        elsif test_subject.vehicle.respond_to? @response_attribute
          print_vehicle_attribute(slots)
        else
          raise InvalidResponseAttribute.new @response_attribute
        end
      end

      def print_slot_attribute(slots)
        puts slots.map {|slot| slot.send(@response_attribute)}.join(', ')
      end

      def print_vehicle_attribute(slots)
        puts slots.map {|slot| slot.vehicle.send(@response_attribute)}.join(', ')
      end
    end
  end
end