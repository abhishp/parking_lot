require_relative File.join('..', '..', 'errors')

module Input
  module Commands
    class UnparkVehicle
      def initialize(slot_number)
        @slot_number = slot_number.to_i
      end

      def process(app_context)
        app_context.parking_lot.unpark_vehicle(@slot_number)
        puts "Slot number #{@slot_number} is free"
      rescue StandardError => e
        puts e.message
      end
    end
  end
end