module Input
  module Commands
    class Invalid
      def initialize(command, *_)
        @command = command
      end

      def process(_)
        puts "Invalid command '#{@command}'"
      end
    end
  end
end