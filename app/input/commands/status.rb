require_relative File.join('..', '..', '..', 'lib', 'utils', 'configuration')


module Input
  module Commands
    class Status
      def initialize
        configs = Utils::Configuration.instance
        columns_char_width = configs.status_columns_char_width
        @slot_num_column_width = columns_char_width[:slot_number]
        @reg_num_column_width = columns_char_width[:registration_number]
        @colour_column_width = columns_char_width[:colour]
      end

      def process(app_context)
        slots = app_context.parking_lot.filled_slots
        if slots.empty?
          puts "Parking lot is empty"
        else
          print_column_titles
          slots.each do |slot|
            puts slot_number(slot) + registration_number(slot) + colour(slot)
          end
        end
      end

      private

      def print_column_titles
        puts 'Slot No.'.ljust(@slot_num_column_width) +
                 'Registration No'.ljust(@reg_num_column_width) +
                 'Colour'.ljust(@colour_column_width)
      end

      def slot_number(slot)
        slot.slot_number.to_s.ljust(@slot_num_column_width)
      end

      def registration_number(slot)
        slot.vehicle.registration_number.to_s.ljust(@reg_num_column_width)
      end

      def colour(slot)
        slot.vehicle.colour.to_s.ljust(@colour_column_width)
      end
    end
  end
end