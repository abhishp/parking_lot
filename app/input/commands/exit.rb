module Input
  module Commands
    class Exit
      def process(_)
        exit(true)
      end
    end
  end
end
