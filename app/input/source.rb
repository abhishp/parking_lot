require_relative 'parser'

module Input
  class Source
    def initialize(source = nil)
      source_stream = String === source ? File.open(source, 'r') : $stdin
      @stream = source_stream
      @parser = Parser.new
    end

    def get_next_command
      command_text = @stream.readline
      @parser.parse(command_text)
    end

    def end?
      @stream.eof?
    end

    def close
      @stream.close
    end
  end
end