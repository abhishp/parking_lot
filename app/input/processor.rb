require_relative File.join('..', 'app_context')

module Input
  class Processor
    def initialize(source)
      @source = source
    end

    def process
      app_context = AppContext.new
      until @source.end?
        command = @source.get_next_command
        command.process(app_context)
      end
      @source.close
    end
  end
end