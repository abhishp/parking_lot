require_relative File.join('..', '..', 'lib', 'utils', 'configuration')
require_relative File.join('commands', 'create_parking_lot')
require_relative File.join('commands', 'exit')
require_relative File.join('commands', 'park_vehicle')
require_relative File.join('commands', 'unpark_vehicle')
require_relative File.join('commands', 'status')
require_relative File.join('commands', 'search')
require_relative File.join('commands', 'invalid')

module Input
  class Parser
    COMMANDS_MAP = {
        create_parking_lot: Commands::CreateParkingLot,
        exit: Commands::Exit,
        park_vehicle: Commands::ParkVehicle,
        unpark_vehicle: Commands::UnparkVehicle,
        show_current_status: Commands::Status,
        search: Commands::Search,
        invalid: Commands::Invalid,
    }

    def initialize
      config = Utils::Configuration.instance
      @commands = config.command_names.invert
      @commands.default_proc = proc do |hash, key|
        cmd = hash.keys.find {|k| key.match(k)}
        if cmd.nil?
          [:invalid, key]
        else
          cmd_args = key.match(cmd) unless cmd.nil?
          [hash[cmd], cmd_args]
        end
      end
    end

    def parse(command)
      cmd_text, *arguments = command.split(' ')
      cmd, cmd_args = @commands[cmd_text]
      arguments.unshift(cmd_args) unless cmd_args.nil?
      COMMANDS_MAP[cmd].new(*arguments)
    end
  end
end