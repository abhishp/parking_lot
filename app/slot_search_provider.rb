require_relative 'models/vehicle/registration_number'

class SlotSearchProvider
  VALID_SEARCH_ATTRIBUTES = [:slot_number, :colour, :registration_number]

  def initialize
    @colour_index = {}
    @registration_number_index = {}
    @slots = []
  end

  def index(slot)
    @slots.push(slot.slot_number)
    index_by_colour(slot)
    index_by_registration_number(slot)
  end

  def remove_from_index(slot_number, vehicle)
    @slots.delete(slot_number)
    remove_from_colour_index(slot_number, vehicle)
    remove_from_registration_number_index(vehicle)
  end

  def find(params = {})
    invalid_search_param = params.keys.find {|param| !VALID_SEARCH_ATTRIBUTES.include?(param)}
    raise InvalidSearchAttribute.new(invalid_search_param) unless invalid_search_param.nil?

    slots = @slots
    slots = filter_on_colour(slots, params[:colour]) if params.has_key?(:colour)
    slots = filter_on_registration_number(slots, params[:registration_number]) if params.has_key?(:registration_number)
    slots = filter_on_slot_number(slots, params[:slot_number]) if params.has_key?(:slot_number)
    slots
  end

  private

  def index_by_colour(slot)
    colour = slot.vehicle.colour.downcase
    @colour_index[colour] ||= []
    @colour_index[colour].push(slot.slot_number)
  end

  def index_by_registration_number(slot)
    @registration_number_index[slot.vehicle.registration_number.to_s] = slot.slot_number
  end

  def remove_from_colour_index(slot_number, vehicle)
    @colour_index[vehicle.colour.downcase].delete(slot_number)
  end

  def remove_from_registration_number_index(vehicle)
    @registration_number_index.delete(vehicle.registration_number)
  end

  def filter_on_colour(slots, colour)
    colours = Array === colour ? colour : [colour]

    filter_on_slot_number slots, @colour_index.values_at(*colours.map(&:downcase)).flatten
  end

  def filter_on_slot_number(slots, slot_number)
    slot_numbers = Array === slot_number ? slot_number : [slot_number]

    slot_numbers.map(&:to_i) & slots
  end

  def filter_on_registration_number(slots, registration_number)
    registration_numbers = Array === registration_number ? registration_number : [registration_number]
    registration_numbers = registration_numbers.map {|rn| Vehicle::RegistrationNumber.new(rn).to_s rescue rn}

    filter_on_slot_number slots, @registration_number_index.values_at(*registration_numbers)
  end
end