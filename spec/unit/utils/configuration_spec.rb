require 'utils/configuration'

TEST_CONFIG_DIR = File.join(__dir__, '..', '..', 'data', 'configs')

module Utils
  describe Configuration do
    before(:each) {RSpec::Mocks::ConstantMutator.stub('CONFIG_DIR', TEST_CONFIG_DIR)}
    let(:parking_lot_full_error_message) {'Parking lot is full'}
    let(:empty_search_result_message) {'No results found'}
    let(:configuration) {Configuration.clone}
    let(:config) {configuration.instance}

    context 'initialization' do
      it 'should read and initialize the default config file' do
        expect(config.to_h).to eq({
                                      commands: {start: 'start', end: 'end'},
                                      foo: 'bar',
                                      status_columns_char_width: {col1: 15, col2: 20},
                                      messages: {
                                          parking_lot_full_error: parking_lot_full_error_message,
                                          empty_search_result: empty_search_result_message
                                      }
                                  })
      end

      context 'with environment overrides' do
        before(:all) do
          @overrides = ['fooVal1', 'fooVal2']
          File.open(File.join(TEST_CONFIG_DIR, 'test.yml'), 'w') do |file|
            file.write({foo: @overrides}.to_yaml)
          end
        end

        after(:all) do
          File.delete(File.join(TEST_CONFIG_DIR, 'test.yml'))
        end

        it 'should override the default configs with environment specific configs' do
          expect(config.to_h[:foo]).to eq @overrides
        end
      end
    end

    context 'instance methods' do
      context '#command_names' do
        it 'should return the configured command names' do
          expect(config.command_names).to eq({start: 'start', end: 'end'})
        end
      end

      context '#status_columns_char_width' do
        it 'should return the configured column widths for status printing' do
          expect(config.status_columns_char_width).to eq({col1: 15, col2: 20})
        end
      end

      context '#parking_lot_full_error_message' do
        it 'should return the configured column widths for status printing' do
          expect(config.parking_lot_full_error_message).to eq parking_lot_full_error_message
        end
      end

      context '#empty_search_result_message' do
        it 'should return the configured column widths for status printing' do
          expect(config.empty_search_result_message).to eq empty_search_result_message
        end
      end
    end
  end
end