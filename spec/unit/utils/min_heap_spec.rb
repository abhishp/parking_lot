require 'utils/min_heap'

module Utils
  describe MinHeap do
    let(:values) {[4, 3, 1, 5, 7, 6, 2]}
    subject {MinHeap.new(values)}

    context 'initialization' do
      it 'should heap-ify the initial array' do
        expect(subject.size).to eq values.size
        expect(subject.top).to eq 1
      end

      it 'should not modify the initial array' do
        expect(values).to eq([4, 3, 1, 5, 7, 6, 2])
      end
    end

    context '#top' do
      it 'should return the minimum element in the heap' do
        expect(subject.top).to eq 1
      end
    end

    context '#pop' do
      it 'should return the minimum element in the heap' do
        expect(subject.pop).to eq 1
        expect(subject.pop).to eq 2
        expect(subject.pop).to eq 3
        expect(subject.pop).to eq 4
      end

      it 'should remove the minimum element' do
        expect {subject.pop}.to change {subject.size}.by -1
      end
    end

    context "#push" do
      it 'should add the value in the heap and adjust the heap' do
        expect(subject.top).to eq 1
        expect {subject.push(8)}.to_not change {subject.top}.from(1)
        expect {subject.push(-1)}.to change {subject.top}.from(1).to(-1)
      end

      it 'should increase the size of heap' do
        expect {subject.push(8)}.to change {subject.size}.by 1
      end
    end

    context '#empty' do
      it 'should be false when heap is not empty' do
        expect(subject).to_not be_empty
      end

      it 'should be true when heap is empty' do
        values.size.times {subject.pop}
        expect(subject).to be_empty
      end
    end
  end
end