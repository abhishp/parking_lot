require 'ext/hash'

describe 'Hash Extensions' do

  describe '#deep_merge' do
    let(:a) {{foo: 'bar', baz: {bag: 'hide'}}}
    let(:b) {{bar: 'foo', baz: {bag: 'blah'}}}
    it 'should have deep_merge instance method' do
      expect(Hash.instance_methods).to include(:merge_deep)
    end

    it 'should merge nested hashes' do
      expect(a.merge_deep(b)).to eq({foo: 'bar', bar: 'foo', 'baz': {bag: 'blah'}})
      expect(b.merge_deep(a)).to eq({foo: 'bar', bar: 'foo', 'baz': {bag: 'hide'}})
    end

    it 'should return a new hash' do
      expect(a.merge_deep(b)).not_to equal(a)
      expect(a.merge_deep(b)).not_to equal(b)
    end
  end

  describe '#symbolize_keys' do
    subject {{'foo': 'bar', baz: 'bag', fub: [{'key': 'value'}, 'val']}}
    it 'should have symbolize_keys instance method' do
      expect(Hash.instance_methods).to include(:symbolize_keys)
    end

    it 'should convert string keys to symbols' do
      expect(subject.symbolize_keys).to eq({foo: 'bar', baz: 'bag', fub: [{key: 'value'}, 'val']})
    end

    it 'should return a new hash' do
      expect(subject.symbolize_keys).not_to equal(subject)
    end

    it 'should allow access with string as well as symbols' do
      symbolized_hash = subject.symbolize_keys

      expect(symbolized_hash['foo']).to eq 'bar'
      expect(symbolized_hash[:foo]).to eq 'bar'
    end
  end

end