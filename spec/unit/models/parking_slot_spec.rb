require 'models/parking_slot'
require 'models/vehicle'

describe ParkingSlot do
  let(:slot_number) {1}
  let(:car) {Vehicle.new('HR-26-A-0001', 'red')}
  subject {ParkingSlot.new slot_number}

  context 'initialization' do
    it 'should set the slot number' do
      expect(subject.slot_number).to eq slot_number
    end

    context 'validation' do
      it 'should raise InvalidSlotNumberError when slot number is less than 1' do
        [0, -1].each do |invalid_slot_number|
          expect {ParkingSlot.new invalid_slot_number}.to raise_error(InvalidSlotNumberError, "Invalid slot number '#{invalid_slot_number}'")
        end
      end

      it 'should not raise any error for positive slot numbers' do
        [1, 10].each do |valid_slot_number|
          expect {ParkingSlot.new valid_slot_number}.to_not raise_error
        end
      end
    end
  end

  context '#park_vehicle' do
    it 'should park the vehicle in the slot' do
      expect {subject.park_vehicle(car)}.to_not raise_error
      expect(subject.vehicle).to eq car
    end

    it 'should raise InvalidVehicleError when no vehicle is given' do
      expect {subject.park_vehicle(nil)}.to raise_error(InvalidVehicleError, "Invalid vehicle of type 'NilClass'")
    end

    it 'should raise InvalidVehicleError when invalid vehicle is given' do
      [double('Vehicle'), 1, 'Vehicle'].each do |invalid_vehicle|
        expect {subject.park_vehicle(invalid_vehicle)}.to raise_error(InvalidVehicleError, "Invalid vehicle of type '#{invalid_vehicle.class}'")
      end
    end

    it 'should raise SlotAlreadyOccupiedError when slot is already occupied' do
      subject.park_vehicle(car)

      expect do
        subject.park_vehicle(Vehicle.new('HR26A0002', 'red'))
      end.to raise_error(SlotAlreadyOccupiedError, 'Cannot park vehicle!! Parking slot #1 is already occupied')
    end
  end

  context '#occupied?' do
    it 'should return false when no vehicle is parked in the slot' do
      expect(subject).to_not be_occupied
    end

    it 'should return true when vehicle is parked in the slot' do
      subject.park_vehicle(car)

      expect(subject).to be_occupied
    end
  end

  context '#unpark_vehicle' do
    before(:each) {subject.park_vehicle(car)}
    it 'should unpark the vehicle' do
      expect {subject.unpark_vehicle}.to change {subject.vehicle}.from(car).to(nil)
    end

    it 'should return the parked vehicle' do
      expect(subject.unpark_vehicle).to eq car
    end

    it 'should raise SlotAlreadyEmptyError when slot is already empty' do
      subject.unpark_vehicle

      expect{subject.unpark_vehicle}.to raise_error SlotAlreadyEmptyError, 'Cannot unpark vehicle!! Parking slot #1 is already empty'
    end
  end
end