require 'models/vehicle/registration_number'

describe Vehicle::RegistrationNumber do
  context 'initialization' do
    it 'should initialize state, district, prefix and number' do
      rn = Vehicle::RegistrationNumber.new('HR-01-A-0001')

      expect(rn.state_code).to eq 'HR'
      expect(rn.district_code).to eq '01'
      expect(rn.prefix).to eq 'A'
      expect(rn.number).to eq '0001'
    end

    context 'validation' do
      let(:invalid_registration_numbers) {['invalid registration number', 'HAR 123', 'HR 01 A 1', 'HR 0 1']}
      let(:valid_registration_numbers) {['HR-26-CA-0001', 'HR-26-CA0001', 'HR-26CA0001', 'HR26-CA-0001', 'HR26CA-0001',
                                         'HR26CA0001', 'HR 26 CA 0001', 'HR 26 CAA 0001', 'HR 1 A 0001', 'hr-26-ca-0001',
                                         'hr-26-ca-333']}

      it 'should raise InvalidRegistrationNumberError for invalid registration number' do
        invalid_registration_numbers.each do |invalid_registration_number|
          expect do
            Vehicle::RegistrationNumber.new(invalid_registration_number)
          end.to raise_error(InvalidRegistrationNumberError, "Invalid registration number '#{invalid_registration_number}'")
        end
      end

      it 'should not raise any error for valid registration number' do
        valid_registration_numbers.each do |valid_registration_number|
          expect {Vehicle::RegistrationNumber.new(valid_registration_number)}.to_not raise_error
        end
      end
    end
  end

  context 'instance methods' do
    subject {Vehicle::RegistrationNumber.new('hr26A0001')}
    context '#to_s' do
      it 'should return the string rep of registration number in hyphenated form' do
        expect(subject.to_s).to eq('HR-26-A-0001')
      end
    end

    context '#==' do
      it 'should return true when their string representation is same' do
        expect(subject).to eq Vehicle::RegistrationNumber.new('HR-26 A0001')
        expect(subject).to eq 'HR-26-A-0001'
        expect(subject).to eq 'hr-26-a-0001'
      end

      it 'should return false when their string representation does not match' do
        expect(subject).to_not eq Vehicle::RegistrationNumber.new('HR-26 A0002')
        expect(subject).to_not eq 'hr-01-a-0001'
      end
    end
  end
end