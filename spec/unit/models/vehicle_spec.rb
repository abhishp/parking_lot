require 'models/vehicle'

describe Vehicle do
  let(:registration_number) {'HR-26-CA-0001'}
  let(:colour) {'red'}

  context 'initialization' do

    it 'should create vehicle with registration number and colour' do
      vehicle = Vehicle.new registration_number, colour

      expect(vehicle.registration_number.to_s).to eq registration_number
      expect(vehicle.colour).to eq colour
    end

    context 'validation' do
      let(:invalid_registration_number) {'invalid registration number'}

      it 'should raise InvalidRegistrationNumberError for invalid registration number' do
        expect do
          Vehicle.new(invalid_registration_number, colour)
        end.to raise_error(InvalidRegistrationNumberError, "Invalid registration number '#{invalid_registration_number}'")
      end
    end
  end

  it 'should not allow to modify registration number' do
    vehicle = Vehicle.new registration_number, colour

    expect {vehicle.registration_number = 'HR-26-A-0001'}.to raise_error(NoMethodError)
  end

  context "#==" do
    subject {Vehicle.new 'HR-01-A-0001', 'Red'}
    it 'should return true when registration number and colour are same' do
      car2 = Vehicle.new 'HR 01 A 0001', 'red'

      expect(subject).to eq car2
    end

    it 'should return true when registration number and colour are same' do
      car2 = Vehicle.new 'HR 01 A 0002', 'red'
      car3 = Vehicle.new 'HR 01 A 0001', 'White'

      expect(subject).to_not eq car2
      expect(subject).to_not eq car3
    end

  end

end