require 'parking_lot'
require 'utils/min_heap'

describe ParkingLot do
  let(:number_of_slots) {5}
  let(:heap) {double('heap')}

  context 'initialization' do
    it 'should initialize the parking lot with specified number of slots' do
      pl = nil
      expect {pl = ParkingLot.new number_of_slots}.to_not raise_error

      expect(pl.number_of_slots).to eq number_of_slots
    end

    it 'should raise InvalidParkingLotSizeError for invalid slot numbers' do
      [0, -1].each do |invalid_number_of_slots|
        expect do
          ParkingLot.new invalid_number_of_slots
        end.to raise_error(InvalidParkingLotSizeError, "Invalid size #{invalid_number_of_slots} for parking lot")
      end
    end

    it 'should initialize the available parking slots heap' do
      expect(Utils::MinHeap).to receive(:new).once.with([1, 2, 3, 4, 5]).and_return(heap)

      ParkingLot.new number_of_slots
    end

    it 'should initialize the slots cache' do
      pl = ParkingLot.new number_of_slots

      expect(pl.instance_variable_get(:@slots)).to eq({})
    end

    it 'should initialize the search provider' do
      expect(SlotSearchProvider).to receive(:new).once.and_return(double('SearchProvider'))

      pl = ParkingLot.new number_of_slots
    end
  end

  context 'instance methods' do
    let(:slot_number) {2}
    let(:registration_number) {'HR-26-A-0001'}
    let(:colour) {'Red'}
    let(:car) {double('Vehicle', registration_number: registration_number, colour: colour)}
    let(:slot) {double('ParkingSlot', slot_number: slot_number, vehicle: car)}
    let(:search_provider) {double('SlotSearchProvider')}

    subject do
      expect(Utils::MinHeap).to receive(:new).and_return(heap)
      expect(SlotSearchProvider).to receive(:new).and_return(search_provider)

      ParkingLot.new number_of_slots
    end

    context '#full?' do
      it 'should return true when there is space available' do
        expect(heap).to receive(:empty?).once.and_return(false)

        expect(subject).to be_full
      end

      it 'should return false when there is no space available' do
        expect(heap).to receive(:empty?).once.and_return(true)

        expect(subject).to_not be_full
      end
    end

    context '#park_vehicle' do
      def setup_mocks_for_park_vehicle
        expect(heap).to receive(:empty?).once.and_return(false)
        expect(heap).to receive(:pop).once.and_return(slot_number)
        expect(ParkingSlot).to receive(:new).once.with(slot_number).and_return(slot)
        expect(slot).to receive(:park_vehicle).once.with(car)
      end

      it 'should return the slot in which the vehicle is parked' do
        setup_mocks_for_park_vehicle
        allow(search_provider).to receive(:index).with(slot)

        expect(subject.park_vehicle(car)).to eq slot
      end

      it 'should park the vehicle in nearest available parking slot' do
        setup_mocks_for_park_vehicle
        allow(search_provider).to receive(:index).with(slot)

        subject.park_vehicle car

        expect(subject.instance_variable_get(:@slots)[slot_number]).to eq slot
      end

      it 'should index the slot in search provider' do
        setup_mocks_for_park_vehicle
        expect(search_provider).to receive(:index).once.with(slot)

        subject.park_vehicle car
      end

      it 'should raise ParkingLotFullError when the parking lot is full' do
        expect(heap).to receive(:empty?).once.and_return(true)

        expect {subject.park_vehicle(car)}.to raise_error(ParkingLotFullError, 'Parking lot is already full')
      end
    end

    context '#unpark_vehicle' do
      let(:heap) {double('Heap', empty?: false, pop: slot_number)}
      let(:slot) {double('ParkingSlot', park_vehicle: car, vehicle: car, slot_number: slot_number)}
      let(:search_provider) {double('SlotSearchProvider', index: nil)}

      def setup_mocks_for_unpark_vehicle
        allow(ParkingSlot).to receive(:new).and_return(slot)
        subject.park_vehicle car
        expect(heap).to receive(:push).once.with(slot_number)
        expect(slot).to receive(:unpark_vehicle).once.and_return(car)
      end

      it 'should return the vehicle that was parked' do
        setup_mocks_for_unpark_vehicle
        allow(search_provider).to receive(:remove_from_index).with(slot.slot_number, slot.vehicle)

        expect(subject.unpark_vehicle slot_number).to eq car
      end

      it 'should unpark the vehicle from the slot' do
        setup_mocks_for_unpark_vehicle
        allow(search_provider).to receive(:remove_from_index).with(slot.slot_number, slot.vehicle)

        subject.unpark_vehicle slot_number

        expect(subject.instance_variable_get(:@slots)[slot_number]).to be_nil
      end

      it 'should remove the unparked slot from index in search provider' do
        setup_mocks_for_unpark_vehicle
        expect(search_provider).to receive(:remove_from_index).with(slot.slot_number, slot.vehicle)

        subject.unpark_vehicle slot_number
      end

      it 'should raise SlotAlreadyEmptyError when the slot it already empty' do
        expect do
          subject.unpark_vehicle slot_number
        end.to raise_error(SlotAlreadyEmptyError, "Cannot unpark vehicle!! Parking slot ##{slot_number} is already empty")
      end

      it 'should raise InvalidSlotNumberError when the slot number is invalid' do
        [-1, 0, number_of_slots + 1].each do |invalid_slot_number|
          expect do
            subject.unpark_vehicle invalid_slot_number
          end.to raise_error(InvalidSlotNumberError, "Invalid slot number '#{invalid_slot_number}'")
        end
      end
    end

    context '#filled_slots' do
      let(:slot1) {double('ParkingSlot', vehicle: double('Vehicle', registration_number: 'HR-26-A-2001', colour: 'White'), slot_number: 1)}
      let(:slot3) {double('ParkingSlot', vehicle: double('Vehicle', registration_number: 'HR-26-A-2001', colour: 'Black'), slot_number: 3)}

      it 'should return all the occupied slots' do
        subject.instance_variable_set(:@slots, {2 => slot, 1 => slot1, 3 => slot3})

        expect(subject.filled_slots).to eq [slot1, slot, slot3]
      end
    end

    context '#find' do
      let(:car1) {double('Vehicle', registration_number: 'HR-26-A-2001', colour: 'White')}
      let(:car3) {double('Vehicle', registration_number: 'HR-26-A-2003', colour: 'Black')}
      let(:car4) {double('Vehicle', registration_number: 'HR-26-A-2004', colour: 'Black')}
      let(:car5) {double('Vehicle', registration_number: 'HR-26-A-2005', colour: 'Red')}
      let(:slot1) {double('ParkingSlot', vehicle: car1, slot_number: 1)}
      let(:slot3) {double('ParkingSlot', vehicle: car3, slot_number: 3)}
      let(:slot4) {double('ParkingSlot', vehicle: car4, slot_number: 4)}
      let(:slot5) {double('ParkingSlot', vehicle: car5, slot_number: 5)}
      let(:search_params) {{colour: ['white', 'black'], slot_number: ['1', '2', '3', '4']}}

      before(:each) do
        subject.instance_variable_set(:@slots, {2 => slot, 1 => slot1, 3 => slot3, 4 => slot4, 5 => slot5})
      end

      it 'should return all the slots filtered by search params' do
        expect(search_provider).to receive(:find).once.with(search_params).and_return((2..4).to_a)

        expect(subject.find(search_params).sort_by(&:slot_number)).to eq [slot, slot3, slot4]
      end
    end
  end
end