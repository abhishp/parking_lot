require 'slot_search_provider'

class SlotSearchProvider
  attr_reader :slots, :registration_number_index, :colour_index
end

describe SlotSearchProvider do
  subject {SlotSearchProvider.new}

  context 'initialization' do
    it 'should initialize the colour index' do
      expect(subject.colour_index).to eq({})
    end

    it 'should initialize the registration number index' do
      expect(subject.registration_number_index).to eq({})
    end

    it 'should initialize slots cache' do
      expect(subject.slots).to eq []
    end
  end

  context 'instance methods' do
    let(:slot_number) {1}
    let(:colour) {'Red'}
    let(:registration_number) {'HR-01-A-0001'}
    let(:slot) {double('ParkingSlot', slot_number: 1, vehicle: double('Vehicle', registration_number: registration_number, colour: colour))}

    context '#index' do
      it 'should cache the slot number' do
        subject.index(slot)

        expect(subject.slots).to eq [slot_number]
      end

      it 'should index by colour' do
        subject.index(slot)

        expect(subject.colour_index[colour.downcase]).to eq [slot_number]
      end

      it 'should cache the slot number' do
        subject.index(slot)

        expect(subject.registration_number_index[registration_number]).to eq slot_number
      end
    end

    context '#remove_from_index' do
      before(:each) {subject.index(slot)}

      it 'should cache the slot number' do
        subject.remove_from_index(slot.slot_number, slot.vehicle)

        expect(subject.slots).to eq []
      end

      it 'should index by colour' do
        subject.remove_from_index(slot.slot_number, slot.vehicle)

        expect(subject.colour_index[colour.downcase]).to eq []
      end

      it 'should cache the slot number' do
        subject.remove_from_index(slot.slot_number, slot.vehicle)

        expect(subject.registration_number_index).to eq({})
        expect(subject.registration_number_index[registration_number]).to be_nil
      end
    end

    context '#find' do
      let(:car2) {double('Vehicle', registration_number: 'HR-12-A-2002', colour: 'White')}
      let(:car3) {double('Vehicle', registration_number: 'HR-23-A-2003', colour: 'Black')}
      let(:car4) {double('Vehicle', registration_number: 'HR-34-A-2004', colour: 'Black')}
      let(:car5) {double('Vehicle', registration_number: 'HR-45-A-2005', colour: 'Red')}
      let(:slot2) {double('ParkingSlot', vehicle: car2, slot_number: 2)}
      let(:slot3) {double('ParkingSlot', vehicle: car3, slot_number: 3)}
      let(:slot4) {double('ParkingSlot', vehicle: car4, slot_number: 4)}
      let(:slot5) {double('ParkingSlot', vehicle: car5, slot_number: 5)}

      before(:each) do
        subject.index(slot)
        subject.index(slot2)
        subject.index(slot3)
        subject.index(slot4)
        subject.index(slot5)
      end

      it 'should return all cached slot numbers when no params are given' do
        expect(subject.find).to eq [1, 2, 3, 4, 5]
      end

      context 'colour filter' do
        it 'should return all slot number matching the color' do
          expect(subject.find({colour: 'Black'})).to eq [3, 4]
        end

        it 'should filter without considering case of search term' do
          expect(subject.find({colour: 'bLaCk'})).to eq [3, 4]
        end

        it 'should allow filtering by multiple colours' do
          expect(subject.find({colour: ['Black', 'white']})).to eq [3, 4, 2]
        end
      end

      context 'registration number filter' do
        it 'should return the slot number matching the registration number of vehicle' do
          expect(subject.find({registration_number: 'HR-12-A-2002'})).to eq [2]
        end

        ['HR-12-A-2002', 'HR-12-A2002', 'HR-12A2002', 'HR12A2002', 'HR12-A-2002', 'HR12A-2002',
         'HR 12 A 2002', 'HR-12-A 2002', 'HR 12-A-2002', 'HR 12-A 2002', 'HR-12-A-2002', 'hr12a2002'].each do |rnf|
          it "should allow registration number in '#{rnf}' format" do
            expect(subject.find({registration_number: rnf})).to eq [2]
          end
        end

        it 'should allow filtering by multiple registration numbers' do
          registration_numbers = [slot2, slot3, slot4].map(&:vehicle).map(&:registration_number)

          expect(subject.find({registration_number: registration_numbers})).to eq [2, 3, 4]
        end
      end

      context 'slot number filter' do
        it 'should return the slot number' do
          expect(subject.find({slot_number: 2})).to eq [2]
        end

        it 'should allow numbers as string' do
          expect(subject.find({slot_number: '2'})).to eq [2]
        end

        it 'should allow filtering by multiple slot numbers' do
          slot_numbers = [slot2, slot3, slot4].map(&:slot_number)

          expect(subject.find({slot_number: slot_numbers})).to eq slot_numbers
        end
      end

      context 'compound filters(apply filters progressively with AND as the operator between)' do
        it 'should allow colour filter with registration number filter' do
          search_params = {colour: ['Black', 'White'], registration_number: ['HR-12-A-2002', 'HR-34-A-2004']}

          expect(subject.find(search_params)).to eq [2, 4]
        end

        it 'should allow colour filter with slot number filter' do
          search_params = {colour: ['Black', 'White'], slot_number: [1, 4]}

          expect(subject.find(search_params)).to eq [4]
        end

        it 'should allow registration number filter with slot number filter' do
          search_params = {registration_number: ['HR-12-A-2002', 'HR-23-A-2003', 'HR-34-A-2004'], slot_number: [1, 2, 4]}

          expect(subject.find(search_params)).to eq [2, 4]
        end

        it 'should allow mix and match of filters' do
          search_params = {colour: 'Black', registration_number: ['HR-23-A-2003', 'HR-34-A-2004'], slot_number: [1, 3]}

          expect(subject.find(search_params)).to eq [3]
        end
      end

      it 'should raise InvalidSearchAttribute error when search attribute is invalid' do
        ['invalid_search_attr', 'invalid search attribute', 'invalid', 1, -1].each do |invalid_search_attribute|
          search_params = {}
          search_params[invalid_search_attribute] = 'search term'

          expect {subject.find(search_params)}.to raise_error(InvalidSearchAttribute, "Invalid search attribute '#{invalid_search_attribute}'")
        end
      end

      it 'should maintain the result order for multiple value search' do
        search_params = {registration_number: ['HR-23-A-2003', 'HR-12-A-2002', 'HR-34-A-2004']}

        expect(subject.find(search_params)).to eq [3, 2, 4]
      end
    end
  end
end