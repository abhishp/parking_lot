require 'input/parser'
require 'utils/configuration'
require 'input/commands/create_parking_lot'
require 'input/commands/exit'
require 'input/commands/park_vehicle'
require 'input/commands/unpark_vehicle'
require 'input/commands/status'
require 'input/commands/search'
require 'input/commands/invalid'

module Input
  describe Parser do
    let(:commands) {
      {
          create_parking_lot: 'create_parking_lot',
          exit: 'exit',
          park_vehicle: 'park',
          unpark_vehicle: 'leave',
          show_current_status: 'status',
          search: '(?<param1>\w+)_for_(?<param2>\w+)'
      }
    }
    let(:config) {double('Configuration', command_names: commands)}

    before(:each) {expect(Utils::Configuration).to receive(:instance).and_return(config)}

    subject {Parser.new}

    context 'initialization' do
      it 'should initialize commands' do
        expect(subject.instance_variable_get(:@commands)).to eq(commands.invert)
      end
    end

    context 'instance methods' do
      context '#parse' do
        let(:create_parking_lot_command) {double('Commands::CreateParkingLot')}
        let(:exit_command) {double('Commands::Exit')}
        let(:park_vehicle_command) {double('Commands::ParkVehicle')}
        let(:unpark_vehicle_command) {double('Commands::UnparkVehicle')}
        let(:status_command) {double('Commands::Status')}
        let(:search_command) {double('Commands::Search')}
        let(:invalid_command) {double('Commands::Invalid')}

        it 'should parse the create_parking_lot command' do
          expect(Commands::CreateParkingLot).to receive(:new).with('5').and_return(create_parking_lot_command)

          expect(subject.parse('create_parking_lot 5')).to eq create_parking_lot_command
        end

        it 'should parse the exit command' do
          expect(Commands::Exit).to receive(:new).and_return(exit_command)

          expect(subject.parse('exit')).to eq exit_command
        end

        it 'should parse the park_vehicle command' do
          registration_number = 'hr01A0001'
          colour = 'Red'
          expect(Commands::ParkVehicle).to receive(:new).with(registration_number, colour).and_return(park_vehicle_command)

          expect(subject.parse("park #{registration_number} #{colour}")).to eq park_vehicle_command
        end

        it 'should parse the unpark_vehicle command' do
          expect(Commands::UnparkVehicle).to receive(:new).with('1').and_return(unpark_vehicle_command)

          expect(subject.parse('leave 1')).to eq unpark_vehicle_command
        end

        it 'should parse the status command' do
          expect(Commands::Status).to receive(:new).with(no_args).and_return(status_command)

          expect(subject.parse('status')).to eq status_command
        end

        context 'search' do
          RSpec::Matchers.define :match_data do |matched_groups|
            match do |actual|
              matched_groups.all? do |key, value|
                actual[key] == value
              end
            end
          end

          it 'should be able to parse search command' do
            expect(Commands::Search).to receive(:new).and_return(search_command)

            expect(subject.parse('foo_for_bar search term')).to eq(search_command)
          end

          it 'should send the dynamic params in the command text as an argument to command initialization' do
            dynamic_params = {'param1' => 'foo', 'param2' => 'bar'}
            expect(Commands::Search).to receive(:new).once.with(match_data(dynamic_params), 'search', 'term')

            subject.parse('foo_for_bar search term')
          end
        end

        it 'should parse invalid commands' do
          ['Some_Invalid_Command args', 'some_invalid_command arg1, arg_n', 'command_other_than_listed arguments1, argument2'].each do |invalid_command_text|
            expect(Commands::Invalid).to receive(:new).once.with(*invalid_command_text.to_s.split(' ')).and_return(invalid_command)

            expect(subject.parse(invalid_command_text)).to eq(invalid_command)
          end
        end
      end
    end
  end
end
