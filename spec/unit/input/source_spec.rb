require 'input/source'

module Input
  describe Source do
    let(:parser) {double('Parser')}
    let(:file_name) {'file_name.txt'}
    let(:stream) {double('File')}

    context 'initialization' do
      it 'should initialize with standard input as the source by default' do
        allow(Parser).to receive(:new).and_return(parser)
        source = Source.new

        expect(source.instance_variable_get(:@stream)).to equal $stdin
      end

      it 'should open the file for reading and set it as source when a string is given as source' do
        allow(Parser).to receive(:new).and_return(parser)
        expect(File).to receive(:open).once.with(file_name, 'r').and_return(stream)

        source = Source.new(file_name)

        expect(source.instance_variable_get(:@stream)).to equal stream
      end

      it 'should instantiate the parser' do
        expect(Parser).to receive(:new).once.and_return(parser)

        Source.new
      end
    end

    context 'instance methods' do
      let(:command) {double('Command')}

      subject {Source.new(file_name)}

      before(:each) do
        expect(Parser).to receive(:new).once.and_return(parser)
        expect(File).to receive(:open).with(file_name, 'r').and_return(stream)
      end

      context '#get_next_command' do
        it 'should read the next line on the input and return the parsed command' do
          expect(stream).to receive(:readline).once.and_return('command_line')
          expect(parser).to receive(:parse).with('command_line').and_return(command)

          expect(subject.get_next_command).to eq command
        end
      end

      context '#end?' do
        it 'should return true when the source stream has reached eof' do
          expect(stream).to receive(:eof?).and_return(true)

          expect(subject).to be_end
        end

        it 'should return false when the source stream has reached eof' do
          expect(stream).to receive(:eof?).and_return(false)

          expect(subject).to_not be_end
        end
      end

      context '#close' do
        it 'should close the stream for reading' do
          expect(stream).to receive(:close).once

          subject.close
        end
      end
    end
  end
end