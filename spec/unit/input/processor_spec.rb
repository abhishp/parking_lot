require 'input/processor'

module Input
  describe Processor do
    let(:source) {double('Source')}
    subject {Processor.new(source)}

    context 'initialization' do
      it 'should set the source' do
        expect(subject.instance_variable_get(:@source)).to eq source
      end
    end

    context 'instance method' do
      context '#process' do
        let(:app_context) { double('AppContext') }
        let(:command) {double('Command')}

        it 'should instantiate create app context' do
          allow(source).to receive(:end?).thrice.and_return(true)
          allow(source).to receive(:close)

          expect(AppContext).to receive(:new).once.and_return(app_context)

          subject.process
        end

        it 'should process the commands with app context till the source ends' do
          allow(AppContext).to receive(:new).once.and_return(app_context)
          allow(source).to receive(:close)

          expect(source).to receive(:end?).thrice.and_return(false, false, true)
          expect(source).to receive(:get_next_command).twice.and_return(command)
          expect(command).to receive(:process).twice.with(app_context)

          subject.process
        end

        it 'should close the source after execution of the file' do
          allow(AppContext).to receive(:new).once.and_return(app_context)
          allow(source).to receive(:end?).once.and_return(true)

          expect(source).to receive(:close).once

          subject.process
        end
      end
    end
  end
end