require 'input/commands/park_vehicle'
require 'errors'
require 'utils/configuration'

module Input::Commands
  describe ParkVehicle do
    let(:colour) {'Red'}
    let(:registration_number) {'HR-01-A-0001'}
    let(:parking_lot_full_error_message) {'Parking lot is full'}
    let(:configs) {double('Configuration')}

    before(:each) {expect(Utils::Configuration).to receive(:instance).and_return configs}

    subject {ParkVehicle.new registration_number, colour}

    context 'initialization' do
      it 'should set the registration number' do
        allow(configs).to receive(:parking_lot_full_error_message)

        expect(subject.instance_variable_get(:@registration_number)).to eq registration_number
      end

      it 'should set the colour' do
        allow(configs).to receive(:parking_lot_full_error_message)

        expect(subject.instance_variable_get(:@colour)).to eq colour
      end

      it 'should set parking lot full error message' do
        expect(configs).to receive(:parking_lot_full_error_message).and_return(parking_lot_full_error_message)

        expect(subject.instance_variable_get(:@parking_lot_full_error_message)).to eq parking_lot_full_error_message
      end
    end

    context 'instance methods' do
      context '#process' do
        let(:car) {double('Vehicle')}
        let(:parking_lot) {double('ParkingLot')}
        let(:app_context) {double('AppContext', parking_lot: parking_lot)}
        let(:slot_number) {1}
        let(:slot) {double('Slot', slot_number: slot_number)}
        let(:error_message) {'Some error occurred while parking the vehicle'}

        before(:each) {allow(configs).to receive(:parking_lot_full_error_message).and_return(parking_lot_full_error_message)}

        it 'should park the vehicle in the parking lot in app context' do
          expect(Vehicle).to receive(:new).once.with(registration_number, colour).and_return(car)
          expect(parking_lot).to receive(:park_vehicle).and_return(slot)

          subject.process(app_context)
        end

        it 'should print the allocated slot number on successful parking' do
          allow(Vehicle).to receive(:new).and_return(car)
          allow(parking_lot).to receive(:park_vehicle).with(car).and_return(slot)

          expect {subject.process(app_context)}.to output("Allocated slot number: #{slot_number}\n").to_stdout
        end

        it 'should print parking full message when parking is full' do
          allow(Vehicle).to receive(:new).and_return(car)
          allow(parking_lot).to receive(:park_vehicle).and_raise(ParkingLotFullError.new)

          expect {subject.process(app_context)}.to output(parking_lot_full_error_message + "\n").to_stdout
        end

        it 'should print the error message for any standard error output' do
          allow(Vehicle).to receive(:new).and_return(car)
          allow(parking_lot).to receive(:park_vehicle).and_raise(error_message)

          expect {subject.process(app_context)}.to output(error_message + "\n").to_stdout
        end
      end
    end
  end
end