require 'input/commands/create_parking_lot'

module Input
  module Commands
    describe CreateParkingLot do
      let(:number_of_slots) {5}
      subject {CreateParkingLot.new(number_of_slots)}

      context 'initialization' do
        it 'should set the size for the parking lot' do
          expect(subject.instance_variable_get(:@number_of_slots)).to eq number_of_slots
        end

        it 'should convert string number of slots to integer' do
          command = CreateParkingLot.new('5')

          expect(command.instance_variable_get(:@number_of_slots)).to eql 5
        end
      end

      context 'instance methods' do
        context '#process' do
          let(:context) {double('AppContext')}
          let(:parking_lot) {double('ParkingLot')}
          let(:success_message) {"Created a parking lot with #{number_of_slots} slots\n"}
          let(:error_message) {'Error while creating the parking lot'}

          it 'should set the parking lot in the app context' do
            expect(ParkingLot).to receive(:new).once.with(number_of_slots).and_return(parking_lot)
            expect(context).to receive(:parking_lot=).once.with(parking_lot)

            subject.process(context)
          end

          it 'should print the success message' do
            expect(ParkingLot).to receive(:new).once.with(number_of_slots).and_return(parking_lot)
            expect(context).to receive(:parking_lot=).once.with(parking_lot)

            expect {subject.process(context)}.to output(success_message).to_stdout
          end

          it 'should print the error message when there is some error creating the parking lot' do
            expect(ParkingLot).to receive(:new).once.with(number_of_slots).and_raise(error_message)

            expect {subject.process(context)}.to output(error_message + "\n").to_stdout
          end
        end
      end
    end
  end
end