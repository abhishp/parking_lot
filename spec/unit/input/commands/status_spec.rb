require 'input/commands/status'

module Input::Commands
  describe Status do
    let(:slot_number_column_width) {10}
    let(:registration_number_column_width) {16}
    let(:colour_column_with) {0}
    let(:config) {double('Configuration', status_columns_char_width: {
        slot_number: slot_number_column_width,
        registration_number: registration_number_column_width,
        colour: colour_column_with
    })}

    before(:each) {expect(Utils::Configuration).to receive(:instance).and_return(config)}

    subject {Status.new}

    context 'initialization' do
      it 'should set the slot number column width' do
        expect(subject.instance_variable_get(:@slot_num_column_width)).to eq slot_number_column_width
      end
      it 'should set the registration number column width' do
        expect(subject.instance_variable_get(:@reg_num_column_width)).to eq registration_number_column_width
      end
      it 'should set the colour column width' do
        expect(subject.instance_variable_get(:@colour_column_width)).to eq colour_column_with
      end
    end

    context 'instance methods' do
      context '#process' do
        let(:parking_lot) {double('ParkingLot')}
        let(:app_context) {double('AppContext', parking_lot: parking_lot)}

        context 'parking lot is empty' do
          let(:slots) {[]}
          let(:empty_parking_lot_message) {"Parking lot is empty\n"}

          it 'should print empty parking lot message' do
            expect(parking_lot).to receive(:filled_slots).and_return(slots)

            expect {subject.process(app_context)}.to output(empty_parking_lot_message).to_stdout
          end
        end

        context 'parking lot has some vehicles' do
          let(:slot1) {double('ParkingSlot', slot_number: 1, vehicle: double('Vehicle', registration_number: 'HR-01-A-0001', colour: 'Red'))}
          let(:slot2) {double('ParkingSlot', slot_number: 3, vehicle: double('Vehicle', registration_number: 'HR-12-A-0002', colour: 'White'))}
          let(:slot3) {double('ParkingSlot', slot_number: 4, vehicle: double('Vehicle', registration_number: 'HR-23-A-0003', colour: 'Blue'))}
          let(:slots) {[slot1, slot2, slot3]}
          let(:parking_lot_status_summary) {<<-EOTXT
Slot No.  Registration No Colour
1         HR-01-A-0001    Red
3         HR-12-A-0002    White
4         HR-23-A-0003    Blue
EOTXT
}

          it 'should print column titles with configured spacing' do
            allow(parking_lot).to receive(:filled_slots).and_return(slots)

            expect {subject.process(app_context)}.to output(/Slot No\.\s{2}Registration No Colour\n/).to_stdout
          end

          it 'should get the list of all the slots filled in the parking lot' do
            expect(parking_lot).to receive(:filled_slots).and_return(slots)

            subject.process(app_context)
          end

          it 'should print the string representation of slot for each occupied' do
            allow(parking_lot).to receive(:filled_slots).and_return(slots)

            expect {subject.process(app_context)}.to output(parking_lot_status_summary).to_stdout
          end
        end
      end
    end
  end
end