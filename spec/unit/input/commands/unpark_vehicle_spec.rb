require 'input/commands/unpark_vehicle'
require 'errors'

module Input::Commands
  describe UnparkVehicle do
    let(:slot_number) {1}

    subject {UnparkVehicle.new slot_number.to_s}

    context 'initialization' do
      it 'should set the slot number and convert it to integer' do
        expect(subject.instance_variable_get(:@slot_number)).to eq slot_number
      end
    end

    context 'instance methods' do
      context '#process' do
        let(:parking_lot) {double('ParkingLot')}
        let(:app_context) {double('AppContext', parking_lot: parking_lot)}
        let(:error_message) {'Some error occurred while parking the vehicle'}
        let(:success_message) {"Slot number #{slot_number} is free\n"}

        it 'should unpark the vehicle from the specified slot in the parking lot from app context' do
          expect(parking_lot).to receive(:unpark_vehicle).with(slot_number)

          subject.process(app_context)
        end

        it 'should print the freed slot number when successful' do
          allow(parking_lot).to receive(:unpark_vehicle)

          expect {subject.process(app_context)}.to output(success_message).to_stdout
        end


        it 'should print the error message for any standard error output' do
          allow(parking_lot).to receive(:unpark_vehicle).and_raise(error_message)

          expect {subject.process(app_context)}.to output(error_message + "\n").to_stdout
        end
      end
    end
  end
end