require 'input/commands/search'
require 'errors'
require 'utils/configuration'

module Input::Commands
  describe Search do
    let(:search_term) {['SearchTerm']}
    let(:attributes) {double('Attributes')}
    let(:search_attribute) {:colour}
    let(:response_attribute) {:slot_number}
    let(:empty_search_result_message) {'No results found'}
    let(:configs) {double('Configuration', empty_search_result_message: empty_search_result_message)}

    before(:each) {allow(Utils::Configuration).to receive(:instance).and_return(configs)}

    context 'initialization' do
      subject {Search.new attributes, *search_term}

      it 'should set the search params after symbolizing the search attribute' do
        expect(attributes).to receive(:[]).once.with('search_attribute').and_return(search_attribute)
        allow(attributes).to receive(:[]).with('response_attribute')

        expect(subject.instance_variable_get(:@search_params)).to eq({colour: search_term})
      end

      it 'should singularize the search attribute' do
        expect(attributes).to receive(:[]).once.with('search_attribute').and_return('colours')
        allow(attributes).to receive(:[]).with('response_attribute')

        expect(subject.instance_variable_get(:@search_params)).to eq({colour: search_term})
      end

      it 'should set the response attribute after symbolizing the string' do
        allow(attributes).to receive(:[]).with('search_attribute').and_return(search_attribute)
        expect(attributes).to receive(:[]).once.with('response_attribute').and_return(response_attribute)

        expect(subject.instance_variable_get(:@response_attribute)).to eq response_attribute
      end

      it 'should singularize the response attribute' do
        allow(attributes).to receive(:[]).with('search_attribute').and_return(search_attribute)
        expect(attributes).to receive(:[]).once.with('response_attribute').and_return('slot_numbers')

        expect(subject.instance_variable_get(:@response_attribute)).to eq response_attribute
      end

      context 'multiple search terms' do
        let(:search_term) {['search', 'term']}

        it 'should combine them as a space separated search term' do
          expect(attributes).to receive(:[]).once.with('search_attribute').and_return(search_attribute)
          allow(attributes).to receive(:[]).with('response_attribute')

          expect(subject.instance_variable_get(:@search_params)).to eq({colour: [search_term.join(' ')]})
        end

        context 'with commas' do
          let(:search_term) {['search', 'term1,', 'search', 'term2']}

          it 'should separate the search terms' do
            expect(attributes).to receive(:[]).once.with('search_attribute').and_return(search_attribute)
            allow(attributes).to receive(:[]).with('response_attribute')

            expect(subject.instance_variable_get(:@search_params)).to eq({colour: ['search term1', 'search term2']})
          end
        end
      end

      it 'should set the empty search result message' do
        allow(attributes).to receive(:[]).with('search_attribute')
        allow(attributes).to receive(:[]).with('response_attribute')

        expect(subject.instance_variable_get(:@empty_search_result_message)).to eq empty_search_result_message
      end

    end

    context 'instance methods' do
      let(:attributes) {{'search_attribute' => search_attribute, 'response_attribute' => response_attribute}}
      let(:parking_lot) {double('ParkingLot')}
      let(:app_context) {double('AppContext', parking_lot: parking_lot)}
      let(:slot1) {double('ParkingSlot', slot_number: 1, vehicle: double('Vehicle', registration_number: 'HR-01-A-0001', colour: 'Red'))}
      let(:slot2) {double('ParkingSlot', slot_number: 3, vehicle: double('Vehicle', registration_number: 'HR-23-A-0003', colour: 'Black'))}
      let(:slots) {[slot1, slot2]}
      let(:search_params) {{search_attribute => search_term}}

      subject {Search.new attributes, search_term}

      context '#process' do
        it 'should find all the slots with the search attribute having search term' do
          expect(parking_lot).to receive(:find).once.with(search_params).and_return(slots)

          subject.process(app_context)
        end

        context 'output' do
          context 'no slots are found' do
            it 'should print not found message' do
              expect(parking_lot).to receive(:find).once.with(search_params).and_return([])

              expect {subject.process(app_context)}.to output(empty_search_result_message + "\n").to_stdout
            end
          end

          context 'response attribute is a slot attribute' do
            it 'should print the value of response attribute on the slot' do
              allow(parking_lot).to receive(:find).once.with(search_params).and_return(slots)

              expect {subject.process(app_context)}.to output("1, 3\n").to_stdout
            end
          end

          context 'response attribute is a vehicle attribute' do
            let(:response_attribute) {:registration_number}

            it 'should print the value of response attribute on the vehicle in the slot' do
              allow(parking_lot).to receive(:find).once.with(search_params).and_return(slots)

              expect {subject.process(app_context)}.to output("HR-01-A-0001, HR-23-A-0003\n").to_stdout
            end
          end

          context 'invalid response attribute' do
            let(:attributes) {double('Attributes')}

            before(:each) {allow(attributes).to receive(:[]).with('search_attribute').and_return(search_attribute)}

            it 'should print invalid response attribute error message' do
              ['', nil, 1, 'response_attribute', 'something_not_on_either_slot_or_vehicle', 'should be an error'].each do |invalid_response_attribute|
                expect(attributes).to receive(:[]).with('response_attribute').once.and_return(invalid_response_attribute)
                allow(parking_lot).to receive(:find).once.with(search_params).and_return(slots)

                expect do
                  Search.new(attributes, search_term).process(app_context)
                end.to output("Invalid response attribute '#{invalid_response_attribute}'\n").to_stdout
              end
            end
          end

          it 'should print the error message for any standard error' do
            error_message = 'Some error occurred while searching for the slot'
            expect(parking_lot).to receive(:find).once.with(search_params).and_raise(error_message)

            expect {subject.process(app_context)}.to output(error_message + "\n").to_stdout
          end
        end
      end
    end
  end
end