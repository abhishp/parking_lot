require 'input/commands/exit'

module Input
  module Commands
    describe Exit do
      subject {Exit.new}
      let(:app_context) {double('AppContext')}

      context 'instance methods' do
        context '#process' do
          it 'should exit the process with success status' do
            expect { subject.process(app_context)}.to raise_error(SystemExit) do |error|
              expect(error).to be_instance_of(SystemExit)
              expect(error.status).to be_zero
            end
          end
        end
      end
    end
  end
end