require 'input/commands/invalid'

module Input::Commands
  describe Invalid do
    let(:command) {'Some invalid command'}
    subject {Invalid.new command}
    context 'initialization' do
      it 'should set the command' do
        expect(subject.instance_variable_get(:@command)).to eq command
      end
    end

    context 'instance methods' do
      let(:app_context) {double('AppContext')}

      context '#process' do
        it 'should print the invalid command message' do
          expect {subject.process(app_context)}.to output("Invalid command '#{command}'\n").to_stdout
        end
      end
    end
  end
end