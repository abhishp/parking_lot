require 'spec_helper'

RSpec.describe 'Parking Lot', type: :aruba do
  let(:command) { run "parking_lot" }
  before(:each) { command.write("create_parking_lot 3\n") }

  it "can create a parking lot" do
    stop_all_commands
    expect(command.output).to end_with("Created a parking lot with 3 slots\n")
  end

  it "can park a car" do
    command.write("park KA-01-HH-3141 Black\n")
    stop_all_commands
    expect(command.output).to end_with("Allocated slot number: 1\n")
  end

  it "can unpark a car" do
    command.write("park KA-01-HH-3141 Black\n")
    command.write("leave 1\n")
    stop_all_commands
    expect(command.output).to end_with("Slot number 1 is free\n")
  end

  it "can report status" do
    command.write("park KA-01-HH-1234 White\n")
    command.write("park KA-01-HH-3141 Black\n")
    command.write("park KA-01-HH-9999 White\n")
    command.write("status\n")
    stop_all_commands
    expect(command.output).to end_with(<<-EOTXT
Slot No.    Registration No    Colour
1           KA-01-HH-1234      White
2           KA-01-HH-3141      Black
3           KA-01-HH-9999      White
EOTXT
)
  end

  context 'search' do
    before(:each) do
      command.write("park KA-01-HH-1234 White\n")
      command.write("park KA-01-HH-3141 Black\n")
      command.write("park KA-01-HH-9999 White\n")
    end

    context 'with registration number' do
      it 'can search for slot numbers' do
        command.write("slot_number_for_registration_number ka-01-hh-3141\n")
        stop_all_commands

        expect(command.output).to end_with("2\n")
      end

      it 'can search for colour' do
        command.write("colour_for_registration_number ka-01-hh-9999\n")
        stop_all_commands

        expect(command.output).to end_with("White\n")
      end

      it 'can search for multiple registration numbers' do
        command.write("colours_for_cars_with_registration_numbers ka-01-hh-9999, KA 01 hh 3141\n")
        stop_all_commands

        expect(command.output).to end_with("White, Black\n")
      end
    end

    context 'with colour' do
      it 'can search for slot numbers' do
        command.write("slot_number_for_cars_with_colour white\n")
        stop_all_commands

        expect(command.output).to end_with("1, 3\n")
      end

      it 'can search for registration number' do
        command.write("registration_numbers_for_cars_with_colour black\n")
        stop_all_commands

        expect(command.output).to end_with("KA-01-HH-3141\n")
      end

      it 'can search for multiple colours' do
        command.write("registration_numbers_for_cars_with_colours white, black\n")
        stop_all_commands

        expect(command.output).to end_with("KA-01-HH-1234, KA-01-HH-9999, KA-01-HH-3141\n")
      end
    end

    context 'with slot number' do
      it 'can search for colour' do
        command.write("colour_for_slot_number 1\n")
        stop_all_commands

        expect(command.output).to end_with("White\n")
      end

      it 'can search for registration number' do
        command.write("registration_number_for_slot_number 2\n")
        stop_all_commands

        expect(command.output).to end_with("KA-01-HH-3141\n")
      end

      it 'can search for multiple slot numbers' do
        command.write("registration_numbers_for_slot_numbers 3, 2\n")
        stop_all_commands

        expect(command.output).to end_with("KA-01-HH-9999, KA-01-HH-3141\n")
      end
    end
  end

  it 'can handle invalid commands gracefully' do
    command.write("foo_bar 3, 2\n")
    stop_all_commands

    expect(command.output).to end_with("Invalid command 'foo_bar'\n")
  end
end
