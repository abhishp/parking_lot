module Utils
  class MinHeap
    include Enumerable

    attr_reader :size

    alias_method :length, :size

    def initialize(values = [])
      @heap_array = []
      @size = 0
      values.each {|v| push(v)}
    end

    def top
      @heap_array[0]
    end

    def pop
      return if empty?
      @size -= 1
      val = @heap_array.shift
      adjust_one 0
      val
    end

    def push(value)
      @heap_array.unshift value
      @size += 1
      adjust_one 0
    end

    def empty?
      @size <= 0
    end

    private

    def adjust_one(index)
      return unless need_adjustment?(index)

      left_child_val = left_child index
      right_child_val = right_child index
      smaller_child_index = right_child_index index
      smaller_child_index = left_child_index index if right_child_val && left_child_val < right_child_val

      @heap_array[index], @heap_array[smaller_child_index] =
          @heap_array[smaller_child_index], @heap_array[index]

      adjust_one(smaller_child_index)
    end

    def leaf_node?(index)
      index >= @size / 2
    end

    def left_child_index(index)
      2 * index + 1
    end

    def left_child(index)
      @heap_array[left_child_index(index)]
    end

    def right_child_index(index)
      2 * index + 2
    end

    def right_child(index)
      @heap_array[right_child_index(index)]
    end

    def need_adjustment?(index)
      return false if leaf_node?(index)
      right_child_val = right_child index

      return false unless right_child_val
      @heap_array[index] > left_child(index) or @heap_array[index] > right_child_val
    end
  end
end