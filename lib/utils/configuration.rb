require 'yaml'
require 'singleton'
require_relative '../ext/hash'

ENVIRONMENT = ENV['PARKING_LOT_ENV'] || 'development'
CONFIG_DIR = File.join(File.dirname(__FILE__), '..', '..', 'configs')

module Utils
  class Configuration
    include Singleton

    def initialize
      default_config_file_path = File.join(CONFIG_DIR, 'default.yml')
      environment_config_file_path = File.join(CONFIG_DIR, "#{ENVIRONMENT}.yml")
      configuration = YAML.load_file(default_config_file_path)
      env_overrides = {}
      if File.file?(environment_config_file_path)
        env_overrides = YAML.load_file(environment_config_file_path)
      end
      @configs = configuration.merge_deep(env_overrides).symbolize_keys
    end

    def command_names
      @configs[:commands]
    end

    def status_columns_char_width
      @configs[:status_columns_char_width]
    end

    def parking_lot_full_error_message
      @configs[:messages][:parking_lot_full_error]
    end

    def empty_search_result_message
      @configs[:messages][:empty_search_result]
    end

    def to_h
      @configs
    end
  end
end
