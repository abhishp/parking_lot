module HashExtensions
  def merge_deep(other)
    merger = proc do |_, thisValue, thatValue|
      Hash === thisValue && Hash === thatValue ? thisValue.merge(thatValue, &merger) : thatValue
    end
    self.merge(other, &merger)
  end

  def symbolize_keys
    hash = self.transform_keys(&:to_sym)
    hash.transform_values! do |v|
      case v
      when Hash
        v.symbolize_keys
      when Array
        v.map { |x| Hash === x ? x.symbolize_keys : x}
      else
        v
      end
    end
    hash.default_proc = proc { |origHash, key| origHash[key.to_sym] }
    hash
  end
end

Hash.class_exec {prepend HashExtensions}
