# Parking Lot

A simple CLI based automated ticketing system to manage day to day work in a multi-storey parking lot.

## Setup

```bash
$ bin/setup
```

Running above script will ensure 
* you have a ruby 2.6 installed on your system. 
* ruby 2.6 is default<sup>[1](#Appendix)</sup> ruby after the shell reload (by adding to the bash profile).
* create an alias ruby2.6 under /usr/local/bin

## Execution

### Interactive mode

```bash
$ bin/parking_lot
```
### File input mode<sup>[2](#Appendix)</sup>

```bash
$ bin/parking_lot <relative or absolute path of the file>
```

Above commands runs the application in production mode with default configuration.
To modify the configs please see [configuration section](#Configuration).

Application can be run in any other environment using following ways:
* prefix the script with `PARKING_LOT_ENV=<env name>`
* exporting the environment in your shell `$ export PARKING_LOT_ENV=<env name>`
 
## Features

Following commands are supported: 
* `create_parking_lot <number of slots>` : creates the parking lot with specified slots
* `park <registration number> <colour>` : parks the car at the nearest empty slot in the parking lot or display an error message for parking lot full
* `leave <slot number>` : marks the specified slot as empty or display error message if already empty
* `status` : shows the current status of the parking lot
* `registration_numbers_for_cars_with_colour <color1> [, ...<colour n>]` : shows the registration number of all the vehicles having specified color(s)
* `slot_numbers_for_cars_with_colour <color1>[, ...<colour n>]` :  shows the slot number of all the vehicles having specified color(s)
* `colours_for_cars_with_registration_number <reg num 1>[,...<reg num n>]` : shows the colour of vehicles having specified registration number(s)
* `slot_numbers_for_cars_with_registration_number <reg num 1>[,...<reg num n>]` : shows the slot numbers of vehicles having specified registration number(s)
* `colours_for_slot_number <slot num 1>[,...<slot num n>]` : shows the colour of vehicles parked at specified slot number(s)
* `registration_numbers_for_slot_number <slot num 1>[,...<slot num n>]` : shows the registration numbers of vehicles parked at specified slot number(s)
* `exit` : exits the application

## Configuration
`parking_lot` allows easy configuration using a yaml file.
Application configuration can be modified in the following two ways:
1. modify the configurations in `config/defaults.yml` file.
2. add an environment specific configuration file which will override the properties set in the `defaults.yml`.
   e.g. if you are running app in `stage` environment then you can create a file `config/stage.yml` which will override the default configs.
   
Following configurations are supported: 
* `commands` : Following command aliases can be configured: 
    * `create_parking_lot`
    * `park_vehicle`
    * `unpark_vehicle`
    * `show_current_status`
    * `search` : this uses a regex as default to provide very flexible search using a single command
    * `exit`
  
  any of the above command can have multiple aliases specified as a regex.
* `status_columns_char_width` : specifies the min column width for each column in the status display.
    Supported columns are
    * `slot_number` : defaults to 12
    * `registration_number` : defaults to 19
    * `colour` : defaults to 0 (fit to content)
* `messages` : configure various messages in the app
    * `parking_lot_full_error` : the error message to be shown when someone tries to park and the parking lot it full
    * `empty_search_result` : the message to be shown when search yields no results

## Development
To setup the system for development execute the setup script.

### Running tests
* Unit
    ```bash
    $ bin/setup rspec
    ```
* Functional
    ```bash
    $ bin/run_functional_tests
    ```
## Appendix
1. only if ruby is installed by the setup script
2. application automatically exits after processing the file

## Future Scope
   * Make all messages in the system configurable
   * Enhancing search command to allow multiple attribute fetch and search at one time
